from network import DQNetwork
from agent import DQNAgent
import tensorflow as tf
from env import GymWrapperFactory
from worker import Worker
import threading as th
from utils import eval_fn , bookkeeping , join_files
from multiprocessing import cpu_count

tf.app.flags.DEFINE_string ( "env" , 'SpaceInvaders-v0' , "Game env" )
tf.app.flags.DEFINE_integer ( "threads" , cpu_count () , "Number of threads to use" )
tf.app.flags.DEFINE_integer ( "max_steps" , 40000000 , "Total frames across the threads" )
tf.app.flags.DEFINE_integer ( "t_max" , 5 , "Update async learner after X" )

tf.app.flags.DEFINE_string ( "log_dir" , 'logs/' , "Log directory for tensorboard" )
tf.app.flags.DEFINE_integer ( "log_interval" , 1000 , "Log and ckpt after X steps" )
tf.app.flags.DEFINE_integer ( "eval_time" , 1800 , "Eval time in seconds" )

FLAGS = tf.app.flags.FLAGS
tf.logging.set_verbosity ( tf.logging.INFO )
tf.set_random_seed ( 1234 )


def create_agents(writer):
    """

    :param writer:
    :return:
    """
    workers = [ ]

    env = GymWrapperFactory.make ( FLAGS.env )

    with tf.device ( '/cpu:0' ):
        with tf.variable_scope ( 'global' ):
            global_network = DQNetwork ( env.action_size )

        master = DQNAgent ( 'master' , action_size=env.action_size , global_network=global_network ,
                            writer=writer )

        workers.append ( Worker ( env=env , agent=master ) )

        for i in range ( FLAGS.threads - 1 ):
            agent = DQNAgent ( 'worker_{}'.format ( i ) , action_size=env.action_size ,
                               global_network=global_network )

            env = GymWrapperFactory.make ( FLAGS.env )
            workers.append ( Worker ( env=env , agent=agent ) )

        return global_network , workers


def main():
    """

    :return:
    """
    threads = [ ]
    with tf.Session () as sess:
        coord = tf.train.Coordinator ()
        writer = tf.summary.FileWriter ( FLAGS.log_dir )
        global_network , workers = create_agents ( writer )
        saver = tf.train.Saver ( global_network.var_list , max_to_keep=2 )
        try:
            ckpt = tf.train.latest_checkpoint ( FLAGS.log_dir )

            sess.run ( tf.global_variables_initializer () )

            if ckpt:
                tf.logging.info ( 'Loading ckpt {}'.format ( ckpt ) )
                saver.restore ( sess , ckpt )

            for w in workers:
                w_fn = lambda: w.run ( sess , coord=coord )
                t = th.Thread ( target=w_fn )
                t.start ()
                threads.append ( t )

            save_th = th.Thread (
                target=eval_fn ( sess , GymWrapperFactory.make ( FLAGS.env ) , FLAGS.eval_time , global_network ,
                                 coord , saver , writer ) )
            save_th.start ()
            threads.append ( save_th )
            coord.join ( threads )

        except KeyboardInterrupt:
            # this doesn't quite work. It save the latest information
            # but it doens't terminate the try appropriately

            bookkeeping ( sess = sess , saver=saver, writer=writer)
            join_files ( FLAGS.log_dir )
            tf.logging.info ( "Stopping all threads" )
            coord.request_stop()
            return



if __name__ == '__main__':
    main ()
