import tensorflow as tf

from tensorflow.contrib.layers import conv2d , flatten , fully_connected


class DQNetwork ( object ):
    def __init__(self , action_size , w=42 , h=42 , m=4 , h_size=256):


        self.state = tf.placeholder ( 'float32' , shape=[ None , w , h , m ] , name='state' )

        self.action = tf.placeholder ( 'int32' , shape=[ None ] , name='action' )
        self.target = tf.placeholder ( 'float32' , shape=[ None ] , name='target' )


        output = self.base_network(self.state, h_size)

        self.value , self.loss = self.value_network ( action_size,output )

        self.var_list = tf.get_collection ( tf.GraphKeys.GLOBAL_VARIABLES , scope=tf.get_variable_scope ().name )
        self.gradients = tf.gradients ( self.loss , self.var_list )


        if tf.get_variable_scope ().name == 'master':
            self.summary = self.summary_ops (output)

    @staticmethod
    def base_network(s , h_size = 256, num_outputs= 32, k = (3,3) , stride = (2,2), activation=tf.nn.elu):

        c1 = conv2d ( inputs=s , num_outputs=num_outputs , kernel_size= k  , stride=stride ,
                      activation_fn=activation ,
                      padding='same' )
        c2 = conv2d ( inputs=c1 , num_outputs=num_outputs , kernel_size= k  , stride=stride ,
                      activation_fn=activation ,
                      padding='same' )
        c3 = conv2d ( inputs=c2 , num_outputs=num_outputs , kernel_size= k  , stride=stride ,
                      activation_fn=activation ,
                      padding='same' )
        c4 = conv2d ( inputs=c3 , num_outputs=num_outputs , kernel_size= k  , stride=stride ,
                      activation_fn=activation ,
                      padding='same' )
        output= fully_connected ( inputs=flatten ( c4 ) , num_outputs=h_size, activation_fn=activation)

        return output


    def value_network(self , action_size, output):

        value = fully_connected ( inputs=output , num_outputs=action_size, activation_fn=None)

        one_hot = tf.one_hot(self.action, action_size, dtype=tf.float32)
        q_value = tf.reduce_sum(tf.multiply(value, one_hot), reduction_indices=1)

        value_loss = tf.reduce_mean(tf.square(self.target - q_value), name = 'value_loss')

        return value , value_loss

    def summary_ops(self, output):

        scalar_values = [ self.value , self.target ,self.loss, output]
        histogram_values = [ self.action, self.value, self.target]

        for v in scalar_values:
            tf.summary.scalar ( v.name.replace ( ':' , "_" ) + '_mean' , tf.reduce_mean ( v ) )
            tf.summary.scalar ( v.name.replace ( ':' , "_" ) + '_max' , tf.reduce_max ( v ) )
            tf.summary.scalar ( v.name.replace ( ':' , "_" ) + '_min' , tf.reduce_min ( v ) )

        for h in histogram_values:
            tf.summary.histogram ( h.name.replace ( ':' , "_" ) , h )
        tf.summary.image ( 'screen' , self.state )

        summary_ops = tf.get_collection ( tf.GraphKeys.SUMMARIES , scope=tf.get_variable_scope ().name )
        summary_ops = [ s for s in summary_ops if 'global' not in s.name ]

        return tf.summary.merge ( summary_ops )