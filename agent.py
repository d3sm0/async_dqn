import numpy as np
import tensorflow as tf
from network import DQNetwork

from tensorflow.contrib.framework import get_global_step


class DQNAgent:
    def __init__(self , agent_name , action_size , global_network , writer=None):

        with tf.variable_scope ( agent_name ):
            self.agent = DQNetwork ( action_size=action_size )

        self.copy = self.copy_op ( self.agent.var_list , global_network.var_list )

        self.train_step = self.train_op ( self.agent.var_list , self.agent.gradients )

        self.global_network = global_network

        self.action_size = action_size

        self.name = agent_name

        self.eps_min = np.random.choice ( [ 0.1 , 0.01 , 0.05 ] , p=[ 0.4 , 0.3 , 0.3 ] )


        if agent_name == 'master':
            self.writer = writer

        print ( 'Created new agent. Hello {}, with eps {}'.format ( agent_name , self.eps_min ) )

    def update_eps(self , global_step , eps_steps):
        eps = 1 - (global_step / eps_steps) * (1 - self.eps_min)
        return max(eps, self.eps_min)

    def get_action(self , state , eps):

        if eps > np.random.uniform ( 0 , 1 ):
            action = np.random.choice ( self.action_size )
        else:

            action = np.argmax ( self.value ( state ) )
        return action

    def global_value(self , state):

        sess = tf.get_default_session ()
        global_value = sess.run ( self.global_network.value , feed_dict={
            self.global_network.state: state
        } )
        return np.argmax ( global_value )

    def value(self , state):
        sess = tf.get_default_session ()

        value = sess.run ( self.agent.value ,
                           feed_dict={
                               self.agent.state: state
                           } )

        return value

    def sync(self):
        sess = tf.get_default_session ()
        sess.run ( self.copy )

    def get_dict(self , batch):

        batch = np.array ( batch )

        states = batch[ : , 0 ]
        actions = batch[ : , 1 ]
        rewards = batch[ : , 2 ]

        feed_dict = {
            self.agent.state: np.vstack ( states ) ,
            self.agent.action: actions ,
            self.agent.target: rewards
        }
        return feed_dict

    def train(self , feed_dict):
        sess = tf.get_default_session ()
        loss , global_step , _ = sess.run (
            [ self.agent.loss , get_global_step () , self.train_step ] ,
            feed_dict=feed_dict )
        return loss , global_step

    def summarize(self , feed_dict):
        sess = tf.get_default_session ()
        summary = sess.run ( self.agent.summary , feed_dict )
        global_step = sess.run ( get_global_step () )
        self.writer.add_summary ( summary , global_step )
        self.writer.flush ()
        return global_step

    @staticmethod
    def train_op(local_vars , gradients , optim=tf.train.AdamOptimizer ( 0.0001 ) , max_clip=40.0):

        global_step = tf.Variable ( 0 , trainable=False , name='global_step' )

        grads_clipped , _ = tf.clip_by_global_norm ( gradients , max_clip )
        return optim.apply_gradients ( list ( zip ( grads_clipped , local_vars ) ) ,
                                       global_step= global_step)

    @staticmethod
    def copy_op(local_list , global_list):

        ops = [ ]
        for local_var , global_var in zip ( local_list , global_list ):
            ops.append ( global_var.assign ( local_var ) )
        return ops
