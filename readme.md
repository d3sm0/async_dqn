## Async DQN

A Tensorflow implementaion of One-step Deep Q Learning as taken from  [Mnih et al. 2016](https://arxiv.org/pdf/1602.01783.pdf).
Minor modifcation in the structure of the CNN, to make computation faster.

The code has yet to be tested on a full training.

