import shutil
import time
import numpy as np
import os

import tensorflow as tf

def eval_fn(sess, env, save_every , global_network , coord , saver , writer , n_trial=1):

    try:
        while not coord.should_stop ():
            times , rewards = [ ] , [ ]
            for n in range(n_trial):
                r, t = eval_once(sess, env, global_network)

            rewards.append(r)
            times.append(t)
            env.render ( close=True )
            bookkeeping (sess, saver , writer , (np.mean ( rewards ) , np.mean ( times )) )
            time.sleep ( save_every )

    except tf.errors.CancelledError:
        return

def eval_once(sess, env, global_network):

    terminal = False
    s = env.reset_random ()
    reward , t = 0 , 0

    while not terminal:

        action = np.argmax ( sess.run ( global_network.value , feed_dict={global_network.state: s} ) )
        s , r , terminal , _ = env.step ( action )
        try:
            env.render ()
        except Exception as e:
            tf.logging.info ( e )
            pass
        reward += r
        t += 1

    return reward, t

def bookkeeping(sess, saver , writer=None , results=None):

    global_step = sess.run ( tf.contrib.framework.get_global_step () )
    try:
        if results is not None:
            r , t = results
            ep_summary = tf.Summary ()

            ep_summary.value.add ( simple_value=r , tag="eval/total_reward" )
            ep_summary.value.add ( simple_value=t , tag="eval/episode_length" )


            writer.add_summary ( ep_summary , global_step )
            writer.add_graph ( graph=sess.graph , global_step=global_step )
            tf.logging.info('Eval computed, max time {}, max score {}'.format(r,t))
            writer.flush ()

        log_dir = writer.get_logdir()
        saver.save ( sess , log_dir, global_step=global_step )
        tf.logging.info (
            'Model saved, current step {}'.format(global_step))

    except Exception as e:
        tf.logging.info ( 'Something went wrong during saving: '.format ( e ) )


def join_files(log_dir):
    idx = len ( list ( os.walk ( log_dir ) ) )
    dir_name = os.path.join ( log_dir , 'run_{}'.format ( idx ) )
    files = os.listdir ( 'logs/' )
    if dir_name not in files:
        os.makedirs ( dir_name )
    for f in files:
        src = os.path.join ( 'logs/' , f )
        if os.path.isfile ( src ) and not f.startswith ( '.' ):
            dst = os.path.join ( dir_name , f )
            shutil.move ( src , dst )
