import tensorflow as tf

tf.logging.set_verbosity ( tf.logging.INFO )


class Worker:
    def __init__(self , env , agent , eps = 1 ,gamma = 0.99):

        self.agent = agent
        self.env = env
        self.state = self.env.reset_random ()
        self.t = 0
        self.eps = eps
        self.gamma = gamma

    def run(self , sess,coord, max_steps = 40000000, summary_interval = 2000, update_interval = 10000):
        with sess.as_default () , sess.graph.as_default ():
            try:
                while not coord.should_stop ():

                    self.agent.sync ()
                    feed_dict = self.sample (self.eps)

                    loss, global_step = self.agent.train ( feed_dict )
                    self.eps = self.agent.update_eps (global_step, max_steps)

                    if global_step % update_interval == 0 :
                        self.agent.sync ()
                        tf.logging.info('Update global network at step {}'.format(global_step))


                    if self.agent.name == 'master' and self.t % summary_interval == 0:
                        tf.logging.info ( 'Master step  {:d}, current loss {:.3f} with eps {:.3f},'.format ( self.t, loss, self.eps ) )

                    if  global_step > max_steps:
                        tf.logging.info ( 'Hopefully I learnt something...test me...' )
                        coord.request_stop ()
                        return


            except tf.errors.CancelledError:
                return


    def sample(self, eps, t_max = 5, clip = 1):

        batch = [ ]
        for n in range(t_max):
            action = self.agent.get_action(self.state, eps)

            next_state , reward , terminal , _ = self.env.step ( action )
            reward = max ( min ( reward , clip ) , -clip )

            if not terminal:
                reward += self.gamma * self.agent.global_value(next_state)
            batch.append ( [ self.state , action , reward])

            self.state = next_state
            self.t +=1

            if terminal == True:
                self.state  = self.env.reset_random()
                break
        feed_dict = self.agent.get_dict(batch)

        return feed_dict

